package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallMaterial;
import co.pushmall.modules.shop.service.dto.PushMallMaterialDto;
import co.pushmall.modules.shop.service.dto.PushMallMaterialQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-01-09
 */
public interface PushMallMaterialService {

    /**
     * 查询数据分页
     *
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String, Object>
     */
    Map<String, Object> queryAll(PushMallMaterialQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria 条件参数
     * @return List<PushMallMaterialDto>
     */
    List<PushMallMaterialDto> queryAll(PushMallMaterialQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return PushMallMaterialDto
     */
    PushMallMaterialDto findById(String id);

    /**
     * 创建
     *
     * @param resources /
     * @return PushMallMaterialDto
     */
    PushMallMaterialDto create(PushMallMaterial resources);

    /**
     * 编辑
     *
     * @param resources /
     */
    void update(PushMallMaterial resources);

    /**
     * 多选删除
     *
     * @param ids /
     */
    void deleteAll(String[] ids);

    void deleteById(String id);


}

package co.pushmall.modules.shop.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName StoreOrderCartInfo
 * @Author pushmall <610796224@qq.com>
 * @Date 2019/10/14
 **/

@Entity
@Data
@Table(name = "pushmall_store_order_cart_info")
public class StoreOrderCartInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "oid")
    private Integer oid;

    @Column(name = "cart_id")
    private Integer cartId;

    @Column(name = "cart_info")
    private String cartInfo;

    @Column(name = "unique")
    private String unique;


}

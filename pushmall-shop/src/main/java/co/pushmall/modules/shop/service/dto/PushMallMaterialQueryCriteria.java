package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;


/**
 * @author pushmall
 * @date 2020-01-09
 */
@Data
public class PushMallMaterialQueryCriteria {
    @Query
    private String groupId;
}

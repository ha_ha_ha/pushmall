package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallSystemConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-10-10
 */
public interface PushMallSystemConfigRepository extends JpaRepository<PushMallSystemConfig, Integer>, JpaSpecificationExecutor {
    PushMallSystemConfig findByMenuName(String str);
}

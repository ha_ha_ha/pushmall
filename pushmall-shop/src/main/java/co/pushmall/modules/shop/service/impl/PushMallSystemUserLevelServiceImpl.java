package co.pushmall.modules.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import co.pushmall.modules.shop.domain.PushMallUserLevel;
import co.pushmall.modules.shop.repository.PushMallSystemUserLevelRepository;
import co.pushmall.modules.shop.repository.PushMallUserLevelRepository;
import co.pushmall.modules.shop.service.PushMallSystemUserLevelService;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallSystemUserLevelMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemUserLevelServiceImpl implements PushMallSystemUserLevelService {

    private final PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository;

    private final PushMallSystemUserLevelMapper pushMallSystemUserLevelMapper;

    private final PushMallUserLevelRepository pushMallUserLevelRepository;

    public PushMallSystemUserLevelServiceImpl(PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository, PushMallSystemUserLevelMapper pushMallSystemUserLevelMapper, PushMallUserLevelRepository pushMallUserLevelRepository) {
        this.pushMallSystemUserLevelRepository = pushMallSystemUserLevelRepository;
        this.pushMallSystemUserLevelMapper = pushMallSystemUserLevelMapper;
        this.pushMallUserLevelRepository = pushMallUserLevelRepository;
    }

    @Override
    public Map<String, Object> queryAll(PushMallSystemUserLevelQueryCriteria criteria, Pageable pageable) {
        criteria.setIsDel(0);
        Page<PushMallSystemUserLevel> page = pushMallSystemUserLevelRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallSystemUserLevelMapper::toDto));
    }

    @Override
    public List<PushMallSystemUserLevelDTO> queryAll(PushMallSystemUserLevelQueryCriteria criteria) {
        return pushMallSystemUserLevelMapper.toDto(pushMallSystemUserLevelRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallSystemUserLevelDTO findById(Integer id) {
        Optional<PushMallSystemUserLevel> yxSystemUserLevel = pushMallSystemUserLevelRepository.findById(id);
        //ValidationUtil.isNull(yxSystemUserLevel,"PushMallSystemUserLevel","id",id);
        if (yxSystemUserLevel.isPresent()) {
            return pushMallSystemUserLevelMapper.toDto(yxSystemUserLevel.get());
        }
        return new PushMallSystemUserLevelDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemUserLevelDTO create(PushMallSystemUserLevel resources) {
        return pushMallSystemUserLevelMapper.toDto(pushMallSystemUserLevelRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemUserLevel resources) {
        Optional<PushMallSystemUserLevel> optionalPushMallSystemUserLevel = pushMallSystemUserLevelRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallSystemUserLevel, "PushMallSystemUserLevel", "id", resources.getId());
        PushMallSystemUserLevel pushMallSystemUserLevel = optionalPushMallSystemUserLevel.get();
        pushMallSystemUserLevel.copy(resources);
        pushMallSystemUserLevelRepository.save(pushMallSystemUserLevel);

        List<PushMallUserLevel> pushMallUserLevel = pushMallUserLevelRepository.selectOneByLevelid(resources.getId());
        if (ObjectUtil.isNotNull(pushMallUserLevel)) {
            pushMallUserLevelRepository.updateUserLevelDiscount(resources.getId(), resources.getDiscount());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallSystemUserLevelRepository.deleteById(id);
    }
}

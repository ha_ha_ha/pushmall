package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallWechatUser;
import co.pushmall.modules.shop.repository.PushMallWechatUserRepository;
import co.pushmall.modules.shop.service.PushMallWechatUserService;
import co.pushmall.modules.shop.service.dto.PushMallWechatUserDTO;
import co.pushmall.modules.shop.service.dto.PushMallWechatUserQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallWechatUserMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-13
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallWechatUserServiceImpl implements PushMallWechatUserService {

    private final PushMallWechatUserRepository pushMallWechatUserRepository;

    private final PushMallWechatUserMapper pushMallWechatUserMapper;

    public PushMallWechatUserServiceImpl(PushMallWechatUserRepository pushMallWechatUserRepository, PushMallWechatUserMapper pushMallWechatUserMapper) {
        this.pushMallWechatUserRepository = pushMallWechatUserRepository;
        this.pushMallWechatUserMapper = pushMallWechatUserMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallWechatUserQueryCriteria criteria, Pageable pageable) {
        Page<PushMallWechatUser> page = pushMallWechatUserRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallWechatUserMapper::toDto));
    }

    @Override
    public List<PushMallWechatUserDTO> queryAll(PushMallWechatUserQueryCriteria criteria) {
        return pushMallWechatUserMapper.toDto(pushMallWechatUserRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallWechatUserDTO findById(Integer uid) {
        Optional<PushMallWechatUser> yxWechatUser = pushMallWechatUserRepository.findById(uid);
        //ValidationUtil.isNull(yxWechatUser,"PushMallWechatUser","uid",uid);
        if (yxWechatUser.isPresent()) {
            return pushMallWechatUserMapper.toDto(yxWechatUser.get());
        }
        return new PushMallWechatUserDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallWechatUserDTO create(PushMallWechatUser resources) {
        return pushMallWechatUserMapper.toDto(pushMallWechatUserRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallWechatUser resources) {
        Optional<PushMallWechatUser> optionalPushMallWechatUser = pushMallWechatUserRepository.findById(resources.getUid());
        ValidationUtil.isNull(optionalPushMallWechatUser, "PushMallWechatUser", "id", resources.getUid());
        PushMallWechatUser pushMallWechatUser = optionalPushMallWechatUser.get();
        pushMallWechatUser.copy(resources);
        pushMallWechatUserRepository.save(pushMallWechatUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer uid) {
        pushMallWechatUserRepository.deleteById(uid);
    }
}

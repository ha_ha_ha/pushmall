package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-04
 */
public interface PushMallSystemUserLevelRepository extends JpaRepository<PushMallSystemUserLevel, Integer>, JpaSpecificationExecutor {
}

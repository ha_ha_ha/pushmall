package co.pushmall.modules.shop.service.dto;

/**
 * @ClassName ChartDataDTO
 * @Author pushmall
 * @Date 2019/11/25
 **/
//@Data
public interface ChartDataDTO {

    // @Value("#{target.adminCount}")
    Double getNum();

    String getTime();
}

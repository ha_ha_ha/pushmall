package co.pushmall.modules.activity.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.service.PushMallStorePinkService;
import co.pushmall.modules.activity.service.dto.PushMallStorePinkQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Api(tags = "商城:拼团记录管理")
@RestController
@RequestMapping("api")
public class StorePinkController {

    private final PushMallStorePinkService pushMallStorePinkService;

    public StorePinkController(PushMallStorePinkService pushMallStorePinkService) {
        this.pushMallStorePinkService = pushMallStorePinkService;
    }

    @Log("查询记录")
    @ApiOperation(value = "查询记录")
    @GetMapping(value = "/PmStorePink")
    @PreAuthorize("@el.check('admin','YXSTOREPINK_ALL','YXSTOREPINK_SELECT')")
    public ResponseEntity getPushMallStorePinks(PushMallStorePinkQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStorePinkService.queryAll(criteria, pageable), HttpStatus.OK);
    }


}

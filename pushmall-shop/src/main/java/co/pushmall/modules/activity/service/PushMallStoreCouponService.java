package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreCoupon;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-09
 */
//@CacheConfig(cacheNames = "yxStoreCoupon")
public interface PushMallStoreCouponService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCouponQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCouponDTO> queryAll(PushMallStoreCouponQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCouponDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCouponDTO create(PushMallStoreCoupon resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCoupon resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}

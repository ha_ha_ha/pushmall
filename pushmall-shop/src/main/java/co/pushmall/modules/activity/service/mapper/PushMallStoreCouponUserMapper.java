package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallStoreCouponUser;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-10
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreCouponUserMapper extends EntityMapper<PushMallStoreCouponUserDTO, PushMallStoreCouponUser> {

}

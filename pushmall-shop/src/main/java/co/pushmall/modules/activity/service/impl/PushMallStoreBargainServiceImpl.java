package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreBargain;
import co.pushmall.modules.activity.repository.PushMallStoreBargainRepository;
import co.pushmall.modules.activity.service.PushMallStoreBargainService;
import co.pushmall.modules.activity.service.dto.PushMallStoreBargainDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreBargainQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreBargainMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-22
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreBargainServiceImpl implements PushMallStoreBargainService {

    private final PushMallStoreBargainRepository pushMallStoreBargainRepository;

    private final PushMallStoreBargainMapper pushMallStoreBargainMapper;

    public PushMallStoreBargainServiceImpl(PushMallStoreBargainRepository pushMallStoreBargainRepository, PushMallStoreBargainMapper pushMallStoreBargainMappers) {
        this.pushMallStoreBargainRepository = pushMallStoreBargainRepository;
        this.pushMallStoreBargainMapper = pushMallStoreBargainMappers;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreBargainQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreBargain> page = pushMallStoreBargainRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreBargainMapper::toDto));
    }

    @Override
    public List<PushMallStoreBargainDTO> queryAll(PushMallStoreBargainQueryCriteria criteria) {
        return pushMallStoreBargainMapper.toDto(pushMallStoreBargainRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreBargainDTO findById(Integer id) {
        Optional<PushMallStoreBargain> yxStoreBargain = pushMallStoreBargainRepository.findById(id);
        ValidationUtil.isNull(yxStoreBargain, "PushMallStoreBargain", "id", id);
        return pushMallStoreBargainMapper.toDto(yxStoreBargain.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreBargainDTO create(PushMallStoreBargain resources) {
        return pushMallStoreBargainMapper.toDto(pushMallStoreBargainRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreBargain resources) {
        Optional<PushMallStoreBargain> optionalPushMallStoreBargain = pushMallStoreBargainRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreBargain, "PushMallStoreBargain", "id", resources.getId());
        PushMallStoreBargain yxStoreBargain = optionalPushMallStoreBargain.get();
        yxStoreBargain.copy(resources);
        pushMallStoreBargainRepository.save(yxStoreBargain);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreBargainRepository.deleteById(id);
    }
}

package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssueUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-09
 */
public interface PushMallStoreCouponIssueUserRepository extends JpaRepository<PushMallStoreCouponIssueUser, Integer>, JpaSpecificationExecutor {
}

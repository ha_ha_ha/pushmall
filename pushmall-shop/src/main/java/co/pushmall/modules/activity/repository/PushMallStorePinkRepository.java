package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStorePink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-18
 */
public interface PushMallStorePinkRepository extends JpaRepository<PushMallStorePink, Integer>, JpaSpecificationExecutor {
    int countByCid(int cid);

    int countByCidAndKId(int cid, int kid);

    int countByKId(int kid);

    PushMallStorePink findByOrderIdKey(int id);

}

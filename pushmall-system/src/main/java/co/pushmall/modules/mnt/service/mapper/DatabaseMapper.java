package co.pushmall.modules.mnt.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.mnt.domain.Database;
import co.pushmall.modules.mnt.service.dto.DatabaseDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author zhanghouying
 * @date 2019-08-24
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DatabaseMapper extends BaseMapper<DatabaseDto, Database> {

}
